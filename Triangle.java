import java.util.*;

public class Triangle{ // check if triangle is valid
    public static boolean triangleValid(int[] sides){ // check if triangle valid
        int[] check={0,1,2,0,2,1,1,2,0}; // combinations of sides to check
        int i;
        for(i=0;i<9;i+=3){ // make checks
            if(sides[check[i]]+sides[check[i+1]]>sides[check[i+2]]){
                continue;
            }
            return false; // a triangle is not valid
        }
        return true; // a triangle is valid
    }
    public static boolean equilateralValid(int[] sides){
        if(sides[0]==sides[1] && sides[1]==sides[2]){
            return true; // a valid equilateral triangle
        }
        return false;
    }
    public static boolean isoscelesValid(int[] sides){
        if(sides[0]==sides[1] || sides[1]==sides[2] || sides[0]==sides[2]){
            return true; // a valid isosceles triangle
        }
        return false;
    }
    public static boolean scaleneValid(int[] sides){
        if(sides[0]!=sides[1] && sides[0]!=sides[2] && sides[1]!=sides[2]){
            return true; // a valid scalene triangle
        }
        return false;
    }
    public static void main(String args[]){ // read 3 integers as command line arguments
        int[] sides={0,0,0};
        sides[0]=Integer.parseInt(args[0]);
        sides[1]=Integer.parseInt(args[1]);
        sides[2]=Integer.parseInt(args[2]);

        if(!triangleValid(sides)){
            System.out.println("Triangle is not valid");
            System.exit(1);
        }
        if(equilateralValid(sides)){
            System.out.println("Triangle is valid equilateral");
            System.exit(0); // no error
        }
        if(isoscelesValid(sides)){
            System.out.println("Triangle is valid isosceles");
            System.exit(0); // no error
        }
        if(scaleneValid(sides)){
            System.out.println("Triangle is valid scalene");
            System.exit(0); // no error
        }
    }
}
