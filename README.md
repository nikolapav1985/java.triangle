Triangle test
-------------

- An example of testing
- Check if triangle is valid
- If triangle is valid, check if equilateral,isosceles or scalene

Compile
-------

- javac Triangle.java

Run
---

- java Triangle 1 1 1
- example output Triangle is valid equilateral

Test environment
----------------

- javac version 14.0.2
- java version 14.0.2
- os lubuntu 16.04 lts kernel version 4.13.0-36-generic
